<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sign Up</title>
    <link rel="stylesheet" href="../public/css/main.css" />
</head>

<body>
    <div class="wrapper">
        <div class="box">
            <div class="box__title">Login</div>
            <form action="" method="POST">
                <div class="box__inputWrapper">
                    <label for="email">Email</label>
                    <input type="email" id="email" placeholder="your@example.com" autocomplete="off" />
                </div>
                <div class="box__inputWrapper">
                    <label for="psw">Password</label>
                    <input type="password" id="psw" placeholder="Your password" autocomplete="off" />
                </div>
            </form>
            <button type="submit" class="btn">Login</button>
            <div class="box__account">
                Already have an account? <a href="" class="link">Login</a>
            </div>
        </div>
    </div>
</body>

</html>