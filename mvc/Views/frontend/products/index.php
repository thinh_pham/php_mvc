<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Products - Index</title>
    <link rel="stylesheet" href="./public/css/bootstrap.min.css" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
</head>

<style>
@import url("https://fonts.googleapis.com/css2?family=Rokkitt:wght@100;200;300;400;500;600;700;800;900&display=swap");

*,
*::before,
*::after {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
}

*:hover,
*:focus {
    outline: 0;
}

a:hover {
    text-decoration: none !important;
}

html {
    font-size: 62.5%;
}

body {
    font-family: "Rokkitt", sans-serif;
    overflow-x: hidden;
}

.btn__card {
    width: 4rem;
    height: 4rem;
    border-radius: 50%;
    border: 1px solid #fff;
    background-color: #fff;
    font-size: 1.6rem;
    display: flex;
    justify-content: center;
    align-items: center;
    text-decoration: none;
    transition: all 0.3s;
}

.btn__icon {
    color: #000;
    transition: all 0.3s;
}

.btn__card:hover,
.btn__card:focus {
    background-color: rgb(127, 209, 3);
    border: 0.1rem solid rgb(127, 209, 3);
}

.btn__card:hover .btn__icon,
.btn__card:focus .btn__icon {
    color: #fff !important;
    transform: rotate(360deg);
}

.card__item {
    position: relative;
    width: 28rem;
    height: 35rem;
}

.card__pic {
    position: relative;
    width: 100%;
    height: 80%;
    overflow: hidden;
}

.card__pic__setbcg {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

.card__pic__setbcg>img {
    width: 100%;
    height: 100%;
}

.card__pic__hover {
    position: absolute;
    bottom: -5rem;
    left: 50%;
    transform: translateX(-50%);
    text-align: center;
    display: flex;
    transition: all 0.5s;
}

.card__pic:hover .card__pic__hover {
    bottom: 2rem;
}

.card__content {
    width: 100%;
    height: 20%;
    font-size: 1.6rem;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

.card__title {
    font-size: 1.8rem;
    font-weight: lighter;
}

.card__price {
    font-size: 2rem;
    font-weight: bold;
}

.alert.alert-info {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    font-size: 5rem;
}
</style>

<body>
    <div class="container">

        <?php

        if (isset($products) && mysqli_num_rows($products) > 0) {
            echo '<div class="row">';
            while ($product = mysqli_fetch_array($products)) {
                echo '
						<div class="col">
						<div class="card__item">
							<div class="card__pic">
								<div class="card__pic__setbcg">
									<img src="../../../../public/assets/' . $product[4] . '" alt="Plants" />
								</div>
								<div class="card__pic__hover">
									<a href="" class="btn__card mr-3">
										<i class="fa fa-heart btn__icon"></i>
									</a>
									<a href="" class="btn__card mr-3">
										<i class="fa fa-info-circle btn__icon"></i>
									</a>
									<a href="" class="btn__card">
										<i class="fa fa-shopping-cart btn__icon"></i>
									</a>
								</div>
							</div>
							<div class="card__content">
								<div class="card__title">' . $product[1] . '</div>
								<div class="card__price">$' . $product[2] . '</div>
							</div>
						</div>
						</div>
						';
            }
            echo '</div>';
        } else {
            echo '<h1 class="alert alert-info text-center">There are no products.</h1>';
        }

        ?>

    </div>

</body>

</html>